import auto_reject_talk


def test_process():
    """
    GIVEN request talök with duration in minutes
    WHEN process is called
    THEN talk is rejected if duration is longer than 90 minutes or short than 20min
    :return:
    """

    class TalkRequest:
        def __init__(self, status, duration_in_minutes):
            self.duration_in_minutes = duration_in_minutes
            self.status = status

        def reject(self):
            self.status = "REJECTED"

    processed_request = auto_reject_talk.process(TalkRequest("PENDING", 45))
    assert processed_request.status == "PENDING"

    processed_request = auto_reject_talk.process(TalkRequest("PENDING", 15))
    assert processed_request.status == "REJECTED"

    processed_request = auto_reject_talk.process(TalkRequest("PENDING", 120))
    assert processed_request.status == "REJECTED"
