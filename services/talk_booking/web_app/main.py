import pathlib
import uuid

import auto_reject_talk
import sqlalchemy
from fastapi import Depends, FastAPI, Response
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from UnleashClient import UnleashClient

from alembic import config, script
from alembic.runtime import migration
from database import talk_request_db
from models.talk_request import TalkRequest

from .config import load_config
from .requests import AcceptTalkRequest, RejectTalkRequest, SubmitTalkRequest
from .responses import TalkRequestDetails, TalkRequestList

app = FastAPI()
app_config = load_config()
unleash_client = UnleashClient(
    app_config.FEATURE_FLAGS_URL,
    app_config.APP_ENVIRONMENT,
    instance_id=app_config.FEATURE_FLAGS_INSTANCE_ID,
    disable_registration=True,
    disable_metrics=True,
)
unleash_client.initialize_client()


def get_db_session():
    engine = create_engine(app_config.SQLALCHEMY_DATABASE_URI, echo=False)
    db = sessionmaker(bind=engine)()

    try:
        yield db
    finally:
        db.close()


@app.get("/health-check/")
def health_check(response: Response):
    engine = sqlalchemy.create_engine(app_config.SQLALCHEMY_DATABASE_URI)
    alembic_cfg = config.Config()
    alembic_cfg.set_main_option(
        "script_location",
        str(pathlib.Path(__file__).parent.parent.absolute() / "alembic"),
    )
    db_script = script.ScriptDirectory.from_config(alembic_cfg)
    with engine.begin() as conn:
        context = migration.MigrationContext.configure(conn)
        if context.get_current_revision() != db_script.get_current_head():
            response.status_code = 400
            return {"message": "Upgrade the database"}
    return {"message": "OK"}


@app.post("/request-talk/", status_code=201, response_model=TalkRequestDetails)
def request_talk(
    submit_talk_request: SubmitTalkRequest, db_session=Depends(get_db_session)
):
    talk_request = TalkRequest(
        id=str(uuid.uuid4()),
        event_time=submit_talk_request.event_time,
        address=submit_talk_request.address,
        topic=submit_talk_request.topic,
        status="PENDING",
        duration_in_minutes=submit_talk_request.duration_in_minutes,
        requester=submit_talk_request.requester,
    )
    if unleash_client.is_enabled(
        "auto-reject-talk-request", {"userId": talk_request.requester}
    ):
        talk_request = auto_reject_talk.process(talk_request)

    talk_request = talk_request_db.save(db_session, talk_request)
    return talk_request


@app.get("/talk-requests/", response_model=TalkRequestList)
def request_talk_list(db_session=Depends(get_db_session)):
    return {
        "results": [
            talk_request.dict() for talk_request in talk_request_db.list_all(db_session)
        ]
    }


@app.post("/talk-request/accept/", status_code=200, response_model=TalkRequestDetails)
def accept_talk_request(
    accept_talk_request_body: AcceptTalkRequest, db_session=Depends(get_db_session)
):
    talk_request = talk_request_db.get_by_id(db_session, accept_talk_request_body.id)
    talk_request.accept()
    talk_request = talk_request_db.save(db_session, talk_request)
    return talk_request


@app.post("/talk-request/reject/", status_code=200, response_model=TalkRequestDetails)
def reject_talk_request(
    accept_talk_request_body: RejectTalkRequest, db_session=Depends(get_db_session)
):
    talk_request = talk_request_db.get_by_id(db_session, accept_talk_request_body.id)
    talk_request.reject()
    talk_request = talk_request_db.save(db_session, talk_request)
    return talk_request
