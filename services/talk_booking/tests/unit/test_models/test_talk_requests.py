import datetime

from models import Address, TalkRequest


def test_talk_request_attributes():
    """
    GIVEN id, event_time, address, duration in minutes, topic, requester, status
    WHEN TalkRequest is initialized
    THEN it has attributes with the same values provided
    """
    event_time = datetime.datetime.now()
    talk_request = TalkRequest(
        id="request_id",
        event_time=event_time,
        address=Address(
            street="Sunny street 42",
            city="Awesome city",
            state="Best state",
            country="Irelenad",
        ),
        duration_in_minutes=45,
        topic="Python type checking",
        requester="john@doe.com",
        status="PENDING",
    )

    assert talk_request.id == "request_id"
    assert talk_request.event_time == event_time
    assert talk_request.address == Address(
        street="Sunny street 42",
        city="Awesome city",
        state="Best state",
        country="Irelenad",
    )
    assert talk_request.duration_in_minutes == 45
    assert talk_request.topic == "Python type checking"
    assert talk_request.requester == "john@doe.com"
    assert talk_request.status == "PENDING"


def test_talk_request_accept():
    event_time = datetime.datetime.now()
    talk_request = TalkRequest(
        id="request_id",
        event_time=event_time,
        address=Address(
            street="Sunny street 42",
            city="Awesome city",
            state="Best state",
            country="Irelenad",
        ),
        duration_in_minutes=45,
        topic="Python type checking",
        requester="john@doe.com",
        status="PENDING",
    )

    talk_request.accept()

    assert talk_request.status == "ACCEPTED"


def test_talk_request_reject():
    event_time = datetime.datetime.now()
    talk_request = TalkRequest(
        id="request_id",
        event_time=event_time,
        address=Address(
            street="Sunny street 42",
            city="Awesome city",
            state="Best state",
            country="Irelenad",
        ),
        duration_in_minutes=45,
        topic="Python type checking",
        requester="john@doe.com",
        status="PENDING",
    )

    talk_request.reject()

    assert talk_request.status == "REJECTED"
