import psycopg2
import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from migrations import downgrade_migrations, upgrade_migrations
from web_app.config import load_config


@pytest.fixture(scope="session")
def database():
    dsn_parts = load_config().SQLALCHEMY_DATABASE_URI.split("/")
    database_name = dsn_parts[-1]
    dsn = "/".join(
        dsn_parts[:-1] + ["postgres"]
    )  # to login tgo postgres database instead of application one
    con = psycopg2.connect(dsn)
    con.autocommit = True
    cur = con.cursor()
    cur.execute(f"DROP DATABASE IF EXISTS {database_name}")
    cur.execute(f"CREATE DATABASE {database_name}")


@pytest.fixture
def database_session(database):
    dsn = load_config().SQLALCHEMY_DATABASE_URI
    engine = create_engine(dsn, echo=False)
    db = sessionmaker(bind=engine)()
    upgrade_migrations(dsn)
    yield db
    db.close()
    downgrade_migrations(dsn)
    engine.dispose()
