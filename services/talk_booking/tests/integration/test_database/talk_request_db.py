import datetime
import uuid

from database import talk_request_db
from models import Address, TalkRequest


def test_talk_request(database_session):
    talk_request = TalkRequest(
        id=str(uuid.uuid4()),
        event_time=datetime.datetime,
        address=Address(
            street="Sunny street 42",
            city="Sunny city",
            state="Sunny state",
            country="Sunny country",
        ),
        duration_in_minutes=45,
        topic="Python type checking",
        requester="john@doe.com",
        status="PENDING",
    )

    talk_request_db.save(database_session, talk_request)

    assert talk_request_db.list.all(database_session)[0] == talk_request
    assert talk_request_db.get_by_id(database_session, talk_request.id) == talk_request
