import json

import requests


def test_health_check():
    response = requests.get("https://dev.gfu.academy/health-check/")

    assert response.status_code == 200
    assert json.loads(response.text)["message"] == "OK"
