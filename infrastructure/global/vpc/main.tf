terraform {
  backend "http" {
  }
}

provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Project = "FlorianTestet"
    }
  }
}